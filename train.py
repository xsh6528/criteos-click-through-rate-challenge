#!/usr/bin/pypy

from datetime import datetime
from csv import DictReader
import cPickle as pickle
from func import *
from test import test

# parameters #################################################################

train_file = '/data/train_good'  # path to training file
model_file = 'model'

# initialize our model
weights = [0.] * D  # weights
historical_grads = [0.5] * D  # sum of historical gradients

# start training a logistic regression model using one pass sgd
loss_total = 0.
loss_batch = 0.

with open(train_file) as train_file_fd:
    reader = DictReader(train_file_fd, header, delimiter='\t')
    for row_num, row in enumerate(reader, start=1):
        y = 1. if row['Label'] == '1' else 0.
        del row['Label']  # can't let the model peek the answer

        # main training procedure
        # step 1, get the hashed features
        x = get_x(row, D)
        # step 2, get prediction
        p = get_p(x, weights)

        # for progress validation, useless for learning our model
        loss = logloss(p, y)
        loss_total += loss
        loss_batch += loss
        if row_num % batch_size == 0:
            print('%s\tencountered: %d\tcurrent whole logloss: %f\tcurrent batch logloss: %f' % (
                datetime.now(), row_num, loss_total / row_num, loss_batch / batch_size))
            loss_batch = 0.

        # step 3, update model with answer
        weights, historical_grads = update_weights(weights, historical_grads, x, p, y)

print('Saving model ...')
with open(model_file, 'wb') as model_file_fd:
    pickle.dump(weights, model_file_fd)

# testing #######################################################
test(weights)
