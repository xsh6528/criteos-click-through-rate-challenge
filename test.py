from datetime import datetime
from csv import DictReader
import cPickle as pickle
from func import *

# testing #######################################################

model_file = 'model'
test_file = 'test'  # path to testing file
result_file = 'results.txt'


def test(weights):
    print('Predicting ...')
    loss = 0.
    with open(test_file) as test_file_fd:
        reader = DictReader(test_file_fd, header, delimiter='\t')
        for row_num, row in enumerate(reader, start=1):

            y = 1. if row['Label'] == '1' else 0.
            del row['Label']  # can't let the model peek the answer

            # main training procedure

            # step 1, get the hashed features
            x = get_x(row, D)

            # step 2, get prediction
            p = get_p(x, weights)

            # for progress validation, useless for learning our model
            loss += logloss(p, y)

            # report current training loss
            if row_num % batch_size == 0:
                print('{}\tscanned: {}\tcurrent loss: {}'.format(datetime.now(), row_num, loss / row_num))

    print('Generating results.txt ...')
    with open(result_file, 'a') as result_file_fd:
        result_file_fd.write('{}, loss: {}, \n'.format(datetime.now(), loss / row_num))


if __name__ == '__main__':
    print('Loading model ...')
    with open(model_file, 'rb') as model_file_fd:
        weights = pickle.load(model_file_fd)
    test(weights)
