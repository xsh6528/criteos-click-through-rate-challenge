from math import exp, log, sqrt
from mmh3 import hash

# parameters #################################################################

D = 2 ** 24  # number of weights use for learning

lambda1 = 0.
lambda2 = 0.

use_signed = False  # Use signed hash? Set to False for to reduce number of hash calls
use_interaction = False
batch_size = 100000

if use_interaction:
    learning_rate = .004  # learning rate for sgd optimization
else:
    learning_rate = .05  # learning rate for sgd optimization

fudge_factor = 1.e-8  # Fudge factor

header = ['Label', 'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9', 'I10', 'I11', 'I12', 'I13', 'C1', 'C2', 'C3',
          'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19',
          'C20', 'C21', 'C22', 'C23', 'C24', 'C25', 'C26']


# function definitions #######################################################


def logloss(p, y):
    '''
    A. Bounded logloss
    :param p: our prediction
    :param y: real answer
    :return: logarithmic loss of p given y
    '''
    p = max(min(p, 1. - 10e-17), 10e-17)
    return -log(p) if y == 1. else -log(1. - p)


def get_x(row, D):
    '''
    B. Apply hash trick of the original csv row
    for simplicity, we treat both integer and categorical features as categorical
    :param row: a csv dictionary, ex: {'Lable': '1', 'I1': '357', 'I2': '', ...}
    :param D: the max index that we can hash to
    :return: a list of indices that its value is 1
    '''
    indexes = [hash(key + '=' + value) % D for key, value in row.items()]

    if use_interaction:
        indexes_interacted = []
        for i in range(len(indexes)):
            for j in range(i + 1, len(indexes)):
                indexes_interacted.append(indexes[i] ^ indexes[j])  # Creating interactions using XOR
        indexes += indexes_interacted

    x = dict()
    x[0] = 1  # 0 is the index of the bias term
    for index in indexes:
        if index not in x:
            x[index] = 0

        if use_signed:
            x[index] += (1 if (hash(str(index)) % 2) == 1 else -1)  # Disable for speed
        else:
            x[index] += 1

    return x  # x contains indices of features that have a value as number of occurences


def get_p(x, w):
    '''
    C. Get probability estimation on x
    :param x: x: features
    :param w: w: weights
    :return: probability of p(y = 1 | x; w)
    '''
    wTx = 0.
    for i, xi in x.items():
        wTx += w[i] * xi  # w[i] * x[i]
    return 1. / (1. + exp(-max(min(wTx, 50.), -50.)))  # bounded sigmoid


def update_weights(weights, historical_grad, x, p, y):
    '''
    D. Update given model
    :param weight: weights
    :param cache: a counter that counts the number of times we encounter a feature
              this is used for adaptive learning rate
    :param x: feature
    :param p: prediction of our model
    :param y: answer
    :return: w: updated model
    :return: n: updated count
    '''
    for i, xi in x.items():
        # alpha / (sqrt(g) + 1) is the adaptive learning rate heuristic
        # (p - y) * x[i] is the current gradient
        # note that in our case, if i in x then x[i] = 1
        # reg = (lambda1 * ((-1.) if w[i] < 0. else 1.) + lambda2 * w[i]) if i != 0 else 0.
        grad = (p - y) * xi  # + reg
        historical_grad[i] += grad ** 2
        weights[i] -= grad * learning_rate / (sqrt(historical_grad[i]))  # Minimising log loss
    return weights, historical_grad

header = ['Label', 'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9', 'I10', 'I11', 'I12', 'I13', 'C1', 'C2', 'C3',
          'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19',
          'C20', 'C21', 'C22', 'C23', 'C24', 'C25', 'C26']
