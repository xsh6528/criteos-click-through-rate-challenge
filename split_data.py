from itertools import islice
from sklearn.model_selection import train_test_split

full_file = 'train.txt'
train_file = 'train'
test_file = 'test'

with open(full_file, 'r') as full_fd:
    with open(train_file, 'w') as train_fd:
        with open(test_file, 'w') as test_fd:
            count = 0
            while True:
                chunk = list(islice(full_fd, 5000))
                if not chunk:
                    break
                count += 5000
                if count % 1000000 == 0:
                    print('encountered: {}'.format(count))
                chunk_train, chunk_test = train_test_split(chunk, test_size=0.01)
                train_fd.writelines(chunk_train)
                test_fd.writelines(chunk_test)
