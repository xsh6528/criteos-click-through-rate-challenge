from pyspark.sql import SparkSession
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, LogisticRegressionModel

spark = SparkSession.builder.appName("example").getOrCreate()


# split file #####################

file = "train.txt"

# header = ['Label', 'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9', 'I10', 'I11', 'I12', 'I13', 'C1', 'C2', 'C3',
#           'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C16', 'C17', 'C18', 'C19',
#           'C20', 'C21', 'C22', 'C23', 'C24', 'C25', 'C26']

fullData = spark.read.csv(file, sep='\t')

splits = fullData.randomSplit([0.8, 0.2])
trainData, testData = splits[0], splits[1]

testData.coalesce(1).write.csv('test.csv')
trainData.coalesce(1).write.csv('train.csv')
exit(0)

# run model #####################

data = fullData.limit(1000).rdd


def parsePoint(row):
    return LabeledPoint(row[0], row[1:])


row = data.first()



parsedData = data.map(parsePoint)



model = LogisticRegressionWithLBFGS.train(data)

print("finish")
